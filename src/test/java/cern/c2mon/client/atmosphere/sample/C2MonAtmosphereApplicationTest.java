package cern.c2mon.client.atmosphere.sample;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.PropertyPlaceholderAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.WebSocketConnectionManager;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import cern.c2mon.client.atmosphere.sample.C2MonAtmosphereApplication;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = C2MonAtmosphereApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext
public class C2MonAtmosphereApplicationTest {
	@Value("${local.server.port}")
	private int port = 1234;

	@Value("${c2mon.atmo.test.timeout:10}")
	private int TEST_TIMEOUT = 10;
	
	@Value("${server.contextPath:}")
	private String contextPath;

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigIn() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Test
	public void atmoEndpoint() throws Exception {
		ConfigurableApplicationContext context = new SpringApplicationBuilder(ClientConfiguration.class,
				PropertyPlaceholderAutoConfiguration.class)
						.properties("websocket.uri:ws://localhost:" + this.port + this.contextPath +"/broadcast"
								  ,"test.timeout:"+TEST_TIMEOUT)
						.run("--spring.main.web_environment=false");
		long count = context.getBean(ClientConfiguration.class).latch.getCount();
		AtomicReference<String> messagePayloadReference = context.getBean(ClientConfiguration.class).messagePayload;
		context.close();
		assertThat(count, equalTo(0L));
		assertThat(messagePayloadReference.get(), containsString("add"));
		
		// Extra sleep time to allow ActiveMQ connector to conclude
		Thread.sleep(2000L);
	}

	@Configuration
	static class ClientConfiguration implements CommandLineRunner {
		private static Log logger = LogFactory.getLog(C2MonAtmosphereApplicationTest.class);
		@Value("${websocket.uri}")
		private String webSocketUri;
		
		@Value("${test.timeout}")
		private int testTimeout;

		private final CountDownLatch latch = new CountDownLatch(1);

		private final AtomicReference<String> messagePayload = new AtomicReference<String>();

		@Override
		public void run(String... args) throws Exception {
			logger.info("Waiting for response: latch=" + this.latch.getCount() + " (timeout " + testTimeout + "s)");
			if (this.latch.await(testTimeout, TimeUnit.SECONDS)) {
				logger.info("Got response: " + this.messagePayload.get());
			} else {
				logger.info("Response not received: latch=" + this.latch.getCount());
			}
		}

		@Bean
		public WebSocketConnectionManager wsConnectionManager() {
			WebSocketConnectionManager manager = new WebSocketConnectionManager(client(), handler(), this.webSocketUri);
			manager.setAutoStartup(true);
			return manager;
		}

		@Bean
		public StandardWebSocketClient client() {
			return new StandardWebSocketClient();
		}

		@Bean
		public TextWebSocketHandler handler() {
			return new TextWebSocketHandler() {

				@Override
				public void afterConnectionEstablished(WebSocketSession session) throws Exception {
					session.sendMessage(new TextMessage(
							"{\"add\":[\"dip://dip/acc/LHC/RunControl/Page1\", \"dip://dip/acc/LHC/RunControl/Page2\"]}"));
				}

				@Override
				protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
					logger.info("Received: " + message + " (" + ClientConfiguration.this.latch.getCount() + ")");
					session.close();
					ClientConfiguration.this.messagePayload.set(message.getPayload());
					ClientConfiguration.this.latch.countDown();
				}
			};
		}

	}
}
